-- MySQL dump 10.13  Distrib 5.6.10, for osx10.7 (x86_64)
--
-- Host: localhost    Database: patoo
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abstract_address`
--

DROP TABLE IF EXISTS `abstract_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abstract_address` (
  `dtype` varchar(31) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `apt` varchar(255) DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `postalcode` varchar(255) DEFAULT NULL,
  `street` varchar(255) NOT NULL,
  `town` varchar(30) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `parish` bigint(20) DEFAULT NULL,
  `account` bigint(20) DEFAULT NULL,
  `neighborhood` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_21qcdo1km1ix1c5et0t5u9igf` (`country`),
  KEY `FK_dbdt736w2f1lee64o23dgvc0d` (`parish`),
  KEY `FK_92ln9jlbghakaesivbstio3n0` (`account`),
  KEY `FK_324dijolaq7kogjk2l5cvibgy` (`neighborhood`),
  CONSTRAINT `FK_324dijolaq7kogjk2l5cvibgy` FOREIGN KEY (`neighborhood`) REFERENCES `neighborhood` (`id`),
  CONSTRAINT `FK_21qcdo1km1ix1c5et0t5u9igf` FOREIGN KEY (`country`) REFERENCES `country` (`country_code`),
  CONSTRAINT `FK_92ln9jlbghakaesivbstio3n0` FOREIGN KEY (`account`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_dbdt736w2f1lee64o23dgvc0d` FOREIGN KEY (`parish`) REFERENCES `parish` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abstract_address`
--

LOCK TABLES `abstract_address` WRITE;
/*!40000 ALTER TABLE `abstract_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `abstract_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `salutation` int(11) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `billing_address` bigint(20) DEFAULT NULL,
  `owner` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_l2w6uce86je7ekitkbppfnelw` (`billing_address`),
  KEY `FK_8skvobhovf34aqj9arj4hu38a` (`owner`),
  CONSTRAINT `FK_8skvobhovf34aqj9arj4hu38a` FOREIGN KEY (`owner`) REFERENCES `app_user` (`id`),
  CONSTRAINT `FK_l2w6uce86je7ekitkbppfnelw` FOREIGN KEY (`billing_address`) REFERENCES `abstract_address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_delivery_addresses`
--

DROP TABLE IF EXISTS `account_delivery_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_delivery_addresses` (
  `account` bigint(20) NOT NULL,
  `delivery_addresses` bigint(20) NOT NULL,
  PRIMARY KEY (`account`,`delivery_addresses`),
  UNIQUE KEY `UK_g9xj78r5awlbh4lxb8fjoc69y` (`delivery_addresses`),
  CONSTRAINT `FK_jdyopfy22najai9b6n9dj0rk0` FOREIGN KEY (`account`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_g9xj78r5awlbh4lxb8fjoc69y` FOREIGN KEY (`delivery_addresses`) REFERENCES `abstract_address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_delivery_addresses`
--

LOCK TABLES `account_delivery_addresses` WRITE;
/*!40000 ALTER TABLE `account_delivery_addresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_delivery_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_user`
--

DROP TABLE IF EXISTS `app_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `timezone` varchar(30) DEFAULT NULL,
  `timezone_offset` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_irayhia1ygarvmv7apksctnqn` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_user`
--

LOCK TABLES `app_user` WRITE;
/*!40000 ALTER TABLE `app_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `app_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart`
--

DROP TABLE IF EXISTS `cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `saved_date` datetime DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `owner` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_6ualopu3g2ywd37v7duchnck1` (`owner`),
  CONSTRAINT `FK_6ualopu3g2ywd37v7duchnck1` FOREIGN KEY (`owner`) REFERENCES `app_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart`
--

LOCK TABLES `cart` WRITE;
/*!40000 ALTER TABLE `cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart_item`
--

DROP TABLE IF EXISTS `cart_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `quantity` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `cart` bigint(20) NOT NULL,
  `shop_item` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_r2jecc1qs002c2t8hrw3gtvtn` (`cart`),
  KEY `FK_9wpq1gxbfqtu24g9peney0545` (`shop_item`),
  CONSTRAINT `FK_9wpq1gxbfqtu24g9peney0545` FOREIGN KEY (`shop_item`) REFERENCES `shop_item` (`id`),
  CONSTRAINT `FK_r2jecc1qs002c2t8hrw3gtvtn` FOREIGN KEY (`cart`) REFERENCES `cart` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart_item`
--

LOCK TABLES `cart_item` WRITE;
/*!40000 ALTER TABLE `cart_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `cart_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `country_code` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`country_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country_parishes`
--

DROP TABLE IF EXISTS `country_parishes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_parishes` (
  `country` varchar(255) NOT NULL,
  `parishes` bigint(20) NOT NULL,
  PRIMARY KEY (`country`,`parishes`),
  UNIQUE KEY `UK_m5am1wpeunrh63uo9pfqkl4py` (`parishes`),
  CONSTRAINT `FK_irw261dt58t660am9hwmq1ws1` FOREIGN KEY (`country`) REFERENCES `country` (`country_code`),
  CONSTRAINT `FK_m5am1wpeunrh63uo9pfqkl4py` FOREIGN KEY (`parishes`) REFERENCES `parish` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country_parishes`
--

LOCK TABLES `country_parishes` WRITE;
/*!40000 ALTER TABLE `country_parishes` DISABLE KEYS */;
/*!40000 ALTER TABLE `country_parishes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_order`
--

DROP TABLE IF EXISTS `customer_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `delivered` datetime DEFAULT NULL,
  `dispatched` datetime DEFAULT NULL,
  `fees` decimal(19,2) DEFAULT NULL,
  `requested_delivery_time` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `tax` decimal(19,2) DEFAULT NULL,
  `total` decimal(19,2) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `agent` bigint(20) DEFAULT NULL,
  `owner` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_a0q2bn6x40f1qot1g112pf2ji` (`agent`),
  KEY `FK_e6kfqmah5m5c13ulohg8rj7ve` (`owner`),
  CONSTRAINT `FK_e6kfqmah5m5c13ulohg8rj7ve` FOREIGN KEY (`owner`) REFERENCES `app_user` (`id`),
  CONSTRAINT `FK_a0q2bn6x40f1qot1g112pf2ji` FOREIGN KEY (`agent`) REFERENCES `app_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_order`
--

LOCK TABLES `customer_order` WRITE;
/*!40000 ALTER TABLE `customer_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_order_order_items`
--

DROP TABLE IF EXISTS `customer_order_order_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_order_order_items` (
  `customer_order` bigint(20) NOT NULL,
  `order_items` bigint(20) NOT NULL,
  PRIMARY KEY (`customer_order`,`order_items`),
  UNIQUE KEY `UK_nyquidu32dp1twx4i2q7e7oh4` (`order_items`),
  CONSTRAINT `FK_g5pxdfa8n417aa9bq3pchqx5r` FOREIGN KEY (`customer_order`) REFERENCES `customer_order` (`id`),
  CONSTRAINT `FK_nyquidu32dp1twx4i2q7e7oh4` FOREIGN KEY (`order_items`) REFERENCES `order_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_order_order_items`
--

LOCK TABLES `customer_order_order_items` WRITE;
/*!40000 ALTER TABLE `customer_order_order_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_order_order_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department`
--

DROP TABLE IF EXISTS `department`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department`
--

LOCK TABLES `department` WRITE;
/*!40000 ALTER TABLE `department` DISABLE KEYS */;
/*!40000 ALTER TABLE `department` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `department_products`
--

DROP TABLE IF EXISTS `department_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `department_products` (
  `department` bigint(20) NOT NULL,
  `products` bigint(20) NOT NULL,
  PRIMARY KEY (`department`,`products`),
  UNIQUE KEY `UK_8boked95m8wsxj29r8c5j270v` (`products`),
  CONSTRAINT `FK_1xah7fnq7ciyx59neiobv15fj` FOREIGN KEY (`department`) REFERENCES `department` (`id`),
  CONSTRAINT `FK_8boked95m8wsxj29r8c5j270v` FOREIGN KEY (`products`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `department_products`
--

LOCK TABLES `department_products` WRITE;
/*!40000 ALTER TABLE `department_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `department_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `neighborhood`
--

DROP TABLE IF EXISTS `neighborhood`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neighborhood` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `parish` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_734qyvl6va70ywft8e0i1wsus` (`name`),
  KEY `FK_ssqdn5emaqjc7nqy5rdhk24kc` (`parish`),
  CONSTRAINT `FK_ssqdn5emaqjc7nqy5rdhk24kc` FOREIGN KEY (`parish`) REFERENCES `parish` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `neighborhood`
--

LOCK TABLES `neighborhood` WRITE;
/*!40000 ALTER TABLE `neighborhood` DISABLE KEYS */;
/*!40000 ALTER TABLE `neighborhood` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_item`
--

DROP TABLE IF EXISTS `order_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `price` decimal(19,2) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `customer_order` bigint(20) NOT NULL,
  `details` bigint(20) DEFAULT NULL,
  `shop` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_qrlg5al9jyrwrqhww1oata9wa` (`customer_order`),
  KEY `FK_an0xxviljwwx6gooo3sia1y60` (`details`),
  KEY `FK_bim4lw3k5r0vd5n3a2bsvu4uh` (`shop`),
  CONSTRAINT `FK_an0xxviljwwx6gooo3sia1y60` FOREIGN KEY (`details`) REFERENCES `order_item_detail` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_item`
--

LOCK TABLES `order_item` WRITE;
/*!40000 ALTER TABLE `order_item` DISABLE KEYS */;
INSERT INTO `order_item` VALUES (1,100.00,0,1,NULL,1);
/*!40000 ALTER TABLE `order_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_item_detail`
--

DROP TABLE IF EXISTS `order_item_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_item_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `brand` varchar(255) NOT NULL,
  `dated_product_id` decimal(19,2) DEFAULT NULL,
  `description` varchar(250) DEFAULT NULL,
  `img_path` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `size_units` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `taxable` bit(1) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `order_item` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_sjv91odrns0u6cuaamfmlp3l0` (`order_item`),
  CONSTRAINT `FK_sjv91odrns0u6cuaamfmlp3l0` FOREIGN KEY (`order_item`) REFERENCES `order_item` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_item_detail`
--

LOCK TABLES `order_item_detail` WRITE;
/*!40000 ALTER TABLE `order_item_detail` DISABLE KEYS */;
INSERT INTO `order_item_detail` VALUES (1,'Betty',1.00,'Sweet','','Condensed Milk','1','','',0,1);
/*!40000 ALTER TABLE `order_item_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parish`
--

DROP TABLE IF EXISTS `parish`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parish` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `abbrev` varchar(5) DEFAULT NULL,
  `area_km_sq` double NOT NULL,
  `capital` varchar(100) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `population` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `country` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bt8r3pen6g4vbja1gy75a221r` (`country`),
  CONSTRAINT `FK_bt8r3pen6g4vbja1gy75a221r` FOREIGN KEY (`country`) REFERENCES `country` (`country_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parish`
--

LOCK TABLES `parish` WRITE;
/*!40000 ALTER TABLE `parish` DISABLE KEYS */;
/*!40000 ALTER TABLE `parish` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `b_name_brand_units` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `size_units` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `taxable` bit(1) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `department` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ffiof3a5ftg7etyc70789hg8q` (`b_name_brand_units`),
  KEY `FK_j1smy1y9muuv78c3k2nmb9k0m` (`department`),
  CONSTRAINT `FK_j1smy1y9muuv78c3k2nmb9k0m` FOREIGN KEY (`department`) REFERENCES `department` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_shop_items`
--

DROP TABLE IF EXISTS `product_shop_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_shop_items` (
  `product` bigint(20) NOT NULL,
  `shop_items` bigint(20) NOT NULL,
  PRIMARY KEY (`product`,`shop_items`),
  UNIQUE KEY `UK_7o5i3o8mc4kpcch4hvmayfl3y` (`shop_items`),
  CONSTRAINT `FK_jmf0xp46uvfgx1ysqdgg17kyy` FOREIGN KEY (`product`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_7o5i3o8mc4kpcch4hvmayfl3y` FOREIGN KEY (`shop_items`) REFERENCES `shop_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_shop_items`
--

LOCK TABLES `product_shop_items` WRITE;
/*!40000 ALTER TABLE `product_shop_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_shop_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop`
--

DROP TABLE IF EXISTS `shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `postalcode` varchar(255) DEFAULT NULL,
  `street` varchar(255) NOT NULL,
  `suite` varchar(255) DEFAULT NULL,
  `town` varchar(30) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `neighborhood` bigint(20) DEFAULT NULL,
  `parish` bigint(20) DEFAULT NULL,
  `type` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ksa05ndh95n2bfdwb5wt68541` (`name`),
  KEY `FK_1e1ww0x1kaye8w1k741crqusr` (`country`),
  KEY `FK_js383nd3n5nust3v06vjjs8du` (`neighborhood`),
  KEY `FK_fdfi8mtadysajb1n0gh6vrv5o` (`parish`),
  KEY `FK_1xexpuqhpvdwwki2r4ve85vah` (`type`),
  CONSTRAINT `FK_1xexpuqhpvdwwki2r4ve85vah` FOREIGN KEY (`type`) REFERENCES `shop_type` (`id`),
  CONSTRAINT `FK_1e1ww0x1kaye8w1k741crqusr` FOREIGN KEY (`country`) REFERENCES `country` (`country_code`),
  CONSTRAINT `FK_fdfi8mtadysajb1n0gh6vrv5o` FOREIGN KEY (`parish`) REFERENCES `parish` (`id`),
  CONSTRAINT `FK_js383nd3n5nust3v06vjjs8du` FOREIGN KEY (`neighborhood`) REFERENCES `neighborhood` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop`
--

LOCK TABLES `shop` WRITE;
/*!40000 ALTER TABLE `shop` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_item`
--

DROP TABLE IF EXISTS `shop_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `price` decimal(19,2) DEFAULT NULL,
  `price_check_date` datetime DEFAULT NULL,
  `special` bit(1) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `product` bigint(20) DEFAULT NULL,
  `shop` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tqvbwl2tlhin82at2me1e0xye` (`product`),
  KEY `FK_puqy3nh9i4ar103e17ky1yfbw` (`shop`),
  CONSTRAINT `FK_puqy3nh9i4ar103e17ky1yfbw` FOREIGN KEY (`shop`) REFERENCES `shop` (`id`),
  CONSTRAINT `FK_tqvbwl2tlhin82at2me1e0xye` FOREIGN KEY (`product`) REFERENCES `product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_item`
--

LOCK TABLES `shop_item` WRITE;
/*!40000 ALTER TABLE `shop_item` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_type`
--

DROP TABLE IF EXISTS `shop_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_type`
--

LOCK TABLES `shop_type` WRITE;
/*!40000 ALTER TABLE `shop_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-07 23:51:46
