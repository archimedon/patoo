-- MySQL dump 10.13  Distrib 5.6.10, for osx10.7 (x86_64)
--
-- Host: localhost    Database: patoo
-- ------------------------------------------------------
-- Server version	5.6.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abstract_address`
--

DROP TABLE IF EXISTS `abstract_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abstract_address` (
  `dtype` varchar(31) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `apt` varchar(255) DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `postalcode` varchar(255) DEFAULT NULL,
  `street` varchar(255) NOT NULL,
  `town` varchar(30) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `parish` bigint(20) DEFAULT NULL,
  `account` bigint(20) DEFAULT NULL,
  `neighborhood` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_21qcdo1km1ix1c5et0t5u9igf` (`country`),
  KEY `FK_dbdt736w2f1lee64o23dgvc0d` (`parish`),
  KEY `FK_92ln9jlbghakaesivbstio3n0` (`account`),
  KEY `FK_324dijolaq7kogjk2l5cvibgy` (`neighborhood`),
  CONSTRAINT `FK_324dijolaq7kogjk2l5cvibgy` FOREIGN KEY (`neighborhood`) REFERENCES `neighborhood` (`id`),
  CONSTRAINT `FK_21qcdo1km1ix1c5et0t5u9igf` FOREIGN KEY (`country`) REFERENCES `country` (`country_code`),
  CONSTRAINT `FK_92ln9jlbghakaesivbstio3n0` FOREIGN KEY (`account`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_dbdt736w2f1lee64o23dgvc0d` FOREIGN KEY (`parish`) REFERENCES `parish` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abstract_address`
--

LOCK TABLES `abstract_address` WRITE;
/*!40000 ALTER TABLE `abstract_address` DISABLE KEYS */;
INSERT INTO `abstract_address` VALUES ('BillingAddress',1,'','4152300521','94111','450 Sansome Street, 12th Floor','San Francisco',0,'US',19,1,NULL),('DeliveryAddress',2,'','4153176679','94131','150 Gardenside Drive','San Francisco',0,'JM',11,1,1),('DeliveryAddress',3,'','4152300521','94945','1045 6th Street','Kingston',0,'JM',13,1,11);
/*!40000 ALTER TABLE `abstract_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `salutation` int(11) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `billing_address` bigint(20) DEFAULT NULL,
  `owner` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_l2w6uce86je7ekitkbppfnelw` (`billing_address`),
  KEY `FK_8skvobhovf34aqj9arj4hu38a` (`owner`),
  CONSTRAINT `FK_8skvobhovf34aqj9arj4hu38a` FOREIGN KEY (`owner`) REFERENCES `app_user` (`id`),
  CONSTRAINT `FK_l2w6uce86je7ekitkbppfnelw` FOREIGN KEY (`billing_address`) REFERENCES `abstract_address` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES (1,'2015-07-01 10:11:14','ronniedz@gmail.com','Ronald','Dennison',1,1,NULL,1);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `account_delivery_addresses`
--

DROP TABLE IF EXISTS `account_delivery_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `account_delivery_addresses` (
  `account` bigint(20) NOT NULL,
  `delivery_addresses` bigint(20) NOT NULL,
  PRIMARY KEY (`account`,`delivery_addresses`),
  UNIQUE KEY `UK_g9xj78r5awlbh4lxb8fjoc69y` (`delivery_addresses`),
  CONSTRAINT `FK_jdyopfy22najai9b6n9dj0rk0` FOREIGN KEY (`account`) REFERENCES `account` (`id`),
  CONSTRAINT `FK_g9xj78r5awlbh4lxb8fjoc69y` FOREIGN KEY (`delivery_addresses`) REFERENCES `abstract_address` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account_delivery_addresses`
--

LOCK TABLES `account_delivery_addresses` WRITE;
/*!40000 ALTER TABLE `account_delivery_addresses` DISABLE KEYS */;
/*!40000 ALTER TABLE `account_delivery_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_user`
--

DROP TABLE IF EXISTS `app_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `app_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(32) NOT NULL,
  `timezone` varchar(30) DEFAULT NULL,
  `timezone_offset` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_irayhia1ygarvmv7apksctnqn` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_user`
--

LOCK TABLES `app_user` WRITE;
/*!40000 ALTER TABLE `app_user` DISABLE KEYS */;
INSERT INTO `app_user` VALUES (1,'2015-07-01 10:11:14','ronniedz@gmail.com','password','US/Pacific',-12800,NULL);
/*!40000 ALTER TABLE `app_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `country_code` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`country_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES ('AD','Andorra',NULL),('AE','United Arab Emirates',NULL),('AF','Afghanistan',NULL),('AG','Antigua and Barbuda',NULL),('AI','Anguilla',NULL),('AL','Albania',NULL),('AM','Armenia',NULL),('AN','Netherlands Antilles',NULL),('AO','Angola',NULL),('AQ','Antarctica',NULL),('AR','Argentina',NULL),('AS','American Samoa',NULL),('AT','Austria',NULL),('AU','Australia',NULL),('AW','Aruba',NULL),('AX','Åland Islands',NULL),('AZ','Azerbaijan',NULL),('BA','Bosnia and Herzegovina',NULL),('BB','Barbados',NULL),('BD','Bangladesh',NULL),('BE','Belgium',NULL),('BF','Burkina Faso',NULL),('BG','Bulgaria',NULL),('BH','Bahrain',NULL),('BI','Burundi',NULL),('BJ','Benin',NULL),('BL','Saint Barthélemy',NULL),('BM','Bermuda',NULL),('BN','Brunei',NULL),('BO','Bolivia',NULL),('BQ','British Antarctic Territory',NULL),('BR','Brazil',NULL),('BS','Bahamas',NULL),('BT','Bhutan',NULL),('BV','Bouvet Island',NULL),('BW','Botswana',NULL),('BY','Belarus',NULL),('BZ','Belize',NULL),('CA','Canada',NULL),('CC','Cocos [Keeling] Islands',NULL),('CD','Congo - Kinshasa',NULL),('CF','Central African Republic',NULL),('CG','Congo - Brazzaville',NULL),('CH','Switzerland',NULL),('CI','Côte d’Ivoire',NULL),('CK','Cook Islands',NULL),('CL','Chile',NULL),('CM','Cameroon',NULL),('CN','China',NULL),('CO','Colombia',NULL),('CR','Costa Rica',NULL),('CS','Serbia and Montenegro',NULL),('CT','Canton and Enderbury Islands',NULL),('CU','Cuba',NULL),('CV','Cape Verde',NULL),('CX','Christmas Island',NULL),('CY','Cyprus',NULL),('CZ','Czech Republic',NULL),('DD','East Germany',NULL),('DE','Germany',NULL),('DJ','Djibouti',NULL),('DK','Denmark',NULL),('DM','Dominica',NULL),('DO','Dominican Republic',NULL),('DZ','Algeria',NULL),('EC','Ecuador',NULL),('EE','Estonia',NULL),('EG','Egypt',NULL),('EH','Western Sahara',NULL),('ER','Eritrea',NULL),('ES','Spain',NULL),('ET','Ethiopia',NULL),('FI','Finland',NULL),('FJ','Fiji',NULL),('FK','Falkland Islands',NULL),('FM','Micronesia',NULL),('FO','Faroe Islands',NULL),('FQ','French Southern and Antarctic Territories',NULL),('FR','France',NULL),('FX','Metropolitan France',NULL),('GA','Gabon',NULL),('GB','United Kingdom',NULL),('GD','Grenada',NULL),('GE','Georgia',NULL),('GF','French Guiana',NULL),('GG','Guernsey',NULL),('GH','Ghana',NULL),('GI','Gibraltar',NULL),('GL','Greenland',NULL),('GM','Gambia',NULL),('GN','Guinea',NULL),('GP','Guadeloupe',NULL),('GQ','Equatorial Guinea',NULL),('GR','Greece',NULL),('GS','South Georgia and the South Sandwich Islands',NULL),('GT','Guatemala',NULL),('GU','Guam',NULL),('GW','Guinea-Bissau',NULL),('GY','Guyana',NULL),('HK','Hong Kong SAR China',NULL),('HM','Heard Island and McDonald Islands',NULL),('HN','Honduras',NULL),('HR','Croatia',NULL),('HT','Haiti',NULL),('HU','Hungary',NULL),('ID','Indonesia',NULL),('IE','Ireland',NULL),('IL','Israel',NULL),('IM','Isle of Man',NULL),('IN','India',NULL),('IO','British Indian Ocean Territory',NULL),('IQ','Iraq',NULL),('IR','Iran',NULL),('IS','Iceland',NULL),('IT','Italy',NULL),('JE','Jersey',NULL),('JM','Jamaica',NULL),('JO','Jordan',NULL),('JP','Japan',NULL),('JT','Johnston Island',NULL),('KE','Kenya',NULL),('KG','Kyrgyzstan',NULL),('KH','Cambodia',NULL),('KI','Kiribati',NULL),('KM','Comoros',NULL),('KN','Saint Kitts and Nevis',NULL),('KP','North Korea',NULL),('KR','South Korea',NULL),('KW','Kuwait',NULL),('KY','Cayman Islands',NULL),('KZ','Kazakhstan',NULL),('LA','Laos',NULL),('LB','Lebanon',NULL),('LC','Saint Lucia',NULL),('LI','Liechtenstein',NULL),('LK','Sri Lanka',NULL),('LR','Liberia',NULL),('LS','Lesotho',NULL),('LT','Lithuania',NULL),('LU','Luxembourg',NULL),('LV','Latvia',NULL),('LY','Libya',NULL),('MA','Morocco',NULL),('MC','Monaco',NULL),('MD','Moldova',NULL),('ME','Montenegro',NULL),('MF','Saint Martin',NULL),('MG','Madagascar',NULL),('MH','Marshall Islands',NULL),('MI','Midway Islands',NULL),('MK','Macedonia',NULL),('ML','Mali',NULL),('MM','Myanmar [Burma]',NULL),('MN','Mongolia',NULL),('MO','Macau SAR China',NULL),('MP','Northern Mariana Islands',NULL),('MQ','Martinique',NULL),('MR','Mauritania',NULL),('MS','Montserrat',NULL),('MT','Malta',NULL),('MU','Mauritius',NULL),('MV','Maldives',NULL),('MW','Malawi',NULL),('MX','Mexico',NULL),('MY','Malaysia',NULL),('MZ','Mozambique',NULL),('NA','Namibia',NULL),('NC','New Caledonia',NULL),('NE','Niger',NULL),('NF','Norfolk Island',NULL),('NG','Nigeria',NULL),('NI','Nicaragua',NULL),('NL','Netherlands',NULL),('NO','Norway',NULL),('NP','Nepal',NULL),('NQ','Dronning Maud Land',NULL),('NR','Nauru',NULL),('NT','Neutral Zone',NULL),('NU','Niue',NULL),('NZ','New Zealand',NULL),('OM','Oman',NULL),('PA','Panama',NULL),('PC','Pacific Islands Trust Territory',NULL),('PE','Peru',NULL),('PF','French Polynesia',NULL),('PG','Papua New Guinea',NULL),('PH','Philippines',NULL),('PK','Pakistan',NULL),('PL','Poland',NULL),('PM','Saint Pierre and Miquelon',NULL),('PN','Pitcairn Islands',NULL),('PR','Puerto Rico',NULL),('PS','Palestinian Territories',NULL),('PT','Portugal',NULL),('PU','U.S. Miscellaneous Pacific Islands',NULL),('PW','Palau',NULL),('PY','Paraguay',NULL),('PZ','Panama Canal Zone',NULL),('QA','Qatar',NULL),('RE','Réunion',NULL),('RO','Romania',NULL),('RS','Serbia',NULL),('RU','Russia',NULL),('RW','Rwanda',NULL),('SA','Saudi Arabia',NULL),('SB','Solomon Islands',NULL),('SC','Seychelles',NULL),('SD','Sudan',NULL),('SE','Sweden',NULL),('SG','Singapore',NULL),('SH','Saint Helena',NULL),('SI','Slovenia',NULL),('SJ','Svalbard and Jan Mayen',NULL),('SK','Slovakia',NULL),('SL','Sierra Leone',NULL),('SM','San Marino',NULL),('SN','Senegal',NULL),('SO','Somalia',NULL),('SR','Suriname',NULL),('ST','São Tomé and Príncipe',NULL),('SU','Union of Soviet Socialist Republics',NULL),('SV','El Salvador',NULL),('SY','Syria',NULL),('SZ','Swaziland',NULL),('TC','Turks and Caicos Islands',NULL),('TD','Chad',NULL),('TF','French Southern Territories',NULL),('TG','Togo',NULL),('TH','Thailand',NULL),('TJ','Tajikistan',NULL),('TK','Tokelau',NULL),('TL','Timor-Leste',NULL),('TM','Turkmenistan',NULL),('TN','Tunisia',NULL),('TO','Tonga',NULL),('TR','Turkey',NULL),('TT','Trinidad and Tobago',NULL),('TV','Tuvalu',NULL),('TW','Taiwan',NULL),('TZ','Tanzania',NULL),('UA','Ukraine',NULL),('UG','Uganda',NULL),('UM','U.S. Minor Outlying Islands',NULL),('US','United States',NULL),('UY','Uruguay',NULL),('UZ','Uzbekistan',NULL),('VA','Vatican City',NULL),('VC','Saint Vincent and the Grenadines',NULL),('VD','North Vietnam',NULL),('VE','Venezuela',NULL),('VG','British Virgin Islands',NULL),('VI','U.S. Virgin Islands',NULL),('VN','Vietnam',NULL),('VU','Vanuatu',NULL),('WF','Wallis and Futuna',NULL),('WK','Wake Island',NULL),('WS','Samoa',NULL),('YD','People\'s Democratic Republic of Yemen',NULL),('YE','Yemen',NULL),('YT','Mayotte',NULL),('ZA','South Africa',NULL),('ZM','Zambia',NULL),('ZW','Zimbabwe',NULL),('ZZ','Unknown or Invalid Region',NULL);
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country_parishes`
--

DROP TABLE IF EXISTS `country_parishes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country_parishes` (
  `country` varchar(255) NOT NULL,
  `parishes` bigint(20) NOT NULL,
  PRIMARY KEY (`country`,`parishes`),
  UNIQUE KEY `UK_m5am1wpeunrh63uo9pfqkl4py` (`parishes`),
  CONSTRAINT `FK_irw261dt58t660am9hwmq1ws1` FOREIGN KEY (`country`) REFERENCES `country` (`country_code`),
  CONSTRAINT `FK_m5am1wpeunrh63uo9pfqkl4py` FOREIGN KEY (`parishes`) REFERENCES `parish` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country_parishes`
--

LOCK TABLES `country_parishes` WRITE;
/*!40000 ALTER TABLE `country_parishes` DISABLE KEYS */;
/*!40000 ALTER TABLE `country_parishes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `neighborhood`
--

DROP TABLE IF EXISTS `neighborhood`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `neighborhood` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `parish` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_734qyvl6va70ywft8e0i1wsus` (`name`),
  KEY `FK_ssqdn5emaqjc7nqy5rdhk24kc` (`parish`),
  CONSTRAINT `FK_ssqdn5emaqjc7nqy5rdhk24kc` FOREIGN KEY (`parish`) REFERENCES `parish` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `neighborhood`
--

LOCK TABLES `neighborhood` WRITE;
/*!40000 ALTER TABLE `neighborhood` DISABLE KEYS */;
INSERT INTO `neighborhood` VALUES (1,'Barbican',NULL,11),(2,'Denham Town',NULL,11),(3,'Red Hills',NULL,11),(4,'Stony Hill',NULL,11),(5,'Havendale',NULL,11),(6,'Forest Gardens',NULL,11),(7,'Forest Hills',NULL,11),(8,'Forest Hills Gardens',NULL,11),(9,'Duhaney Park',NULL,11),(10,'Patrick City',NULL,11),(11,'Constant Spring',NULL,11),(12,'Queen Hill',NULL,11),(13,'Meadowbrook Estate',NULL,11),(14,'Pembroke Hall',NULL,11),(15,'Half Way Tree',NULL,11),(16,'Harbour View',NULL,11),(17,'Liguanea',NULL,11),(18,'Mona',NULL,11),(19,'New Kingston',NULL,11),(20,'Norbrook',NULL,11),(21,'Trenchtown',NULL,11);
/*!40000 ALTER TABLE `neighborhood` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parish`
--

DROP TABLE IF EXISTS `parish`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `parish` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `abbrev` varchar(5) DEFAULT NULL,
  `area_km_sq` double NOT NULL,
  `capital` varchar(100) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `population` int(11) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `country` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bt8r3pen6g4vbja1gy75a221r` (`country`),
  CONSTRAINT `FK_bt8r3pen6g4vbja1gy75a221r` FOREIGN KEY (`country`) REFERENCES `country` (`country_code`)
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parish`
--

LOCK TABLES `parish` WRITE;
/*!40000 ALTER TABLE `parish` DISABLE KEYS */;
INSERT INTO `parish` VALUES (1,'',450.4,'Lucea','Hanover',69533,NULL,'JM'),(2,'',1212.4,'Black River','Saint Elizabeth',150205,NULL,'JM'),(3,'',594.9,'Montego Bay','Saint James',183811,NULL,'JM'),(4,'',874.6,'Falmouth','Trelawny',75164,NULL,'JM'),(5,'',807,'Savanna-la-Mar','Westmoreland',144103,NULL,'JM'),(6,'',1196.3,'May Pen','Clarendon',245103,NULL,'JM'),(7,'',830.1,'Mandeville','Manchester',189797,NULL,'JM'),(8,'',1212.6,'Saint Ann\'s Bay','Saint Ann',172362,NULL,'JM'),(9,'',1192.4,'Spanish Town','Saint Catherine',516218,NULL,'JM'),(10,'',610.5,'Port Maria','Saint Mary',113615,NULL,'JM'),(11,'',21.8,'Kingston','Kingston',89057,NULL,'JM'),(12,'',814,'Port Antonio','Portland',81744,NULL,'JM'),(13,'',430.7,'Half Way Tree','Saint Andrew',573369,NULL,'JM'),(14,'',742.8,'Morant Bay','Saint Thomas',93902,NULL,'JM'),(15,'AL',0,'','Alabama',0,NULL,'US'),(16,'AK',0,'','Alaska',0,NULL,'US'),(17,'AZ',0,'','Arizona',0,NULL,'US'),(18,'AR',0,'','Arkansas',0,NULL,'US'),(19,'CA',0,'','California',0,NULL,'US'),(20,'CO',0,'','Colorado',0,NULL,'US'),(21,'CT',0,'','Connecticut',0,NULL,'US'),(22,'DE',0,'','Delaware',0,NULL,'US'),(23,'FL',0,'','Florida',0,NULL,'US'),(24,'GA',0,'','Georgia',0,NULL,'US'),(25,'HI',0,'','Hawaii',0,NULL,'US'),(26,'ID',0,'','Idaho',0,NULL,'US'),(27,'IL',0,'','Illinois',0,NULL,'US'),(28,'IN',0,'','Indiana',0,NULL,'US'),(29,'IA',0,'','Iowa',0,NULL,'US'),(30,'KS',0,'','Kansas',0,NULL,'US'),(31,'KY',0,'','Kentucky',0,NULL,'US'),(32,'LA',0,'','Louisiana',0,NULL,'US'),(33,'ME',0,'','Maine',0,NULL,'US'),(34,'MD',0,'','Maryland',0,NULL,'US'),(35,'MA',0,'','Massachusetts',0,NULL,'US'),(36,'MI',0,'','Michigan',0,NULL,'US'),(37,'MN',0,'','Minnesota',0,NULL,'US'),(38,'MS',0,'','Mississippi',0,NULL,'US'),(39,'MO',0,'','Missouri',0,NULL,'US'),(40,'MT',0,'','Montana',0,NULL,'US'),(41,'NE',0,'','Nebraska',0,NULL,'US'),(42,'NV',0,'','Nevada',0,NULL,'US'),(43,'NH',0,'','New Hampshire',0,NULL,'US'),(44,'NJ',0,'','New Jersey',0,NULL,'US'),(45,'NM',0,'','New Mexico',0,NULL,'US'),(46,'NY',0,'','New York',0,NULL,'US'),(47,'NC',0,'','North Carolina',0,NULL,'US'),(48,'ND',0,'','North Dakota',0,NULL,'US'),(49,'OH',0,'','Ohio',0,NULL,'US'),(50,'OK',0,'','Oklahoma',0,NULL,'US'),(51,'OR',0,'','Oregon',0,NULL,'US'),(52,'PA',0,'','Pennsylvania',0,NULL,'US'),(53,'RI',0,'','Rhode Island',0,NULL,'US'),(54,'SC',0,'','South Carolina',0,NULL,'US'),(55,'SD',0,'','South Dakota',0,NULL,'US'),(56,'TN',0,'','Tennessee',0,NULL,'US'),(57,'TX',0,'','Texas',0,NULL,'US'),(58,'UT',0,'','Utah',0,NULL,'US'),(59,'VT',0,'','Vermont',0,NULL,'US'),(60,'VA',0,'','Virginia',0,NULL,'US'),(61,'WA',0,'','Washington',0,NULL,'US'),(62,'WV',0,'','West Virginia',0,NULL,'US'),(63,'WI',0,'','Wisconsin',0,NULL,'US'),(64,'WY',0,'','Wyoming',0,NULL,'US'),(65,'DC',0,'','Washington DC',0,NULL,'US'),(66,'AA',0,'','Armed Forces Americas',0,NULL,'US'),(67,'AE',0,'','Armed Forces Europe',0,NULL,'US'),(68,'AP',0,'','Armed Forces Pacific',0,NULL,'US');
/*!40000 ALTER TABLE `parish` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `b_name_brand_units` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `description` varchar(250) DEFAULT NULL,
  `image` longblob,
  `name` varchar(255) NOT NULL,
  `size_units` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ffiof3a5ftg7etyc70789hg8q` (`b_name_brand_units`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product`
--

LOCK TABLES `product` WRITE;
/*!40000 ALTER TABLE `product` DISABLE KEYS */;
INSERT INTO `product` VALUES (1,'CondensedMilkBetty','Betty','Sweet!!','','Condensed Milk','1','',0);
/*!40000 ALTER TABLE `product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_shop_items`
--

DROP TABLE IF EXISTS `product_shop_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_shop_items` (
  `product` bigint(20) NOT NULL,
  `shop_items` bigint(20) NOT NULL,
  PRIMARY KEY (`product`,`shop_items`),
  UNIQUE KEY `UK_7o5i3o8mc4kpcch4hvmayfl3y` (`shop_items`),
  CONSTRAINT `FK_jmf0xp46uvfgx1ysqdgg17kyy` FOREIGN KEY (`product`) REFERENCES `product` (`id`),
  CONSTRAINT `FK_7o5i3o8mc4kpcch4hvmayfl3y` FOREIGN KEY (`shop_items`) REFERENCES `shop_item` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_shop_items`
--

LOCK TABLES `product_shop_items` WRITE;
/*!40000 ALTER TABLE `product_shop_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_shop_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop`
--

DROP TABLE IF EXISTS `shop`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `postalcode` varchar(255) DEFAULT NULL,
  `street` varchar(255) NOT NULL,
  `suite` varchar(255) DEFAULT NULL,
  `town` varchar(30) DEFAULT NULL,
  `type` int(11) NOT NULL,
  `version` int(11) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `neighborhood` bigint(20) DEFAULT NULL,
  `parish` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_ksa05ndh95n2bfdwb5wt68541` (`name`),
  KEY `FK_1e1ww0x1kaye8w1k741crqusr` (`country`),
  KEY `FK_js383nd3n5nust3v06vjjs8du` (`neighborhood`),
  KEY `FK_fdfi8mtadysajb1n0gh6vrv5o` (`parish`),
  CONSTRAINT `FK_fdfi8mtadysajb1n0gh6vrv5o` FOREIGN KEY (`parish`) REFERENCES `parish` (`id`),
  CONSTRAINT `FK_1e1ww0x1kaye8w1k741crqusr` FOREIGN KEY (`country`) REFERENCES `country` (`country_code`),
  CONSTRAINT `FK_js383nd3n5nust3v06vjjs8du` FOREIGN KEY (`neighborhood`) REFERENCES `neighborhood` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop`
--

LOCK TABLES `shop` WRITE;
/*!40000 ALTER TABLE `shop` DISABLE KEYS */;
INSERT INTO `shop` VALUES (1,'Sovreign','4152300521','94945','1045 6th Street',NULL,'Novato',0,0,'JM',1,1),(2,'King\'s','4153176679','','450 Constant Spring Road','','Kingston 6',0,0,'JM',11,13);
/*!40000 ALTER TABLE `shop` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_item`
--

DROP TABLE IF EXISTS `shop_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `price` decimal(19,2) DEFAULT NULL,
  `price_check_date` datetime DEFAULT NULL,
  `version` int(11) DEFAULT NULL,
  `product` bigint(20) DEFAULT NULL,
  `shop` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tqvbwl2tlhin82at2me1e0xye` (`product`),
  KEY `FK_puqy3nh9i4ar103e17ky1yfbw` (`shop`),
  CONSTRAINT `FK_puqy3nh9i4ar103e17ky1yfbw` FOREIGN KEY (`shop`) REFERENCES `shop` (`id`),
  CONSTRAINT `FK_tqvbwl2tlhin82at2me1e0xye` FOREIGN KEY (`product`) REFERENCES `product` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_item`
--

LOCK TABLES `shop_item` WRITE;
/*!40000 ALTER TABLE `shop_item` DISABLE KEYS */;
INSERT INTO `shop_item` VALUES (1,520.00,'2015-07-05 00:00:00',0,1,2),(2,410.00,'2015-07-05 00:00:00',0,1,1);
/*!40000 ALTER TABLE `shop_item` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-07-05  3:27:56
