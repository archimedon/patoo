#!/usr/bin/env bash
####################################

usage ()
{
      cat <<ENDOFMESSAGE
Usage:

NAME
	run_app.sh -- An example documenting the steps to running the app.

SYNOPSIS
	run_app.sh [-b [-d app_dir]] [-il] [-j [-p port_num]] [-h]

DESCRIPTION
	

	Options:

	  -b  If set, package and deploy the war
	  -d app_dir - use with -b. 
	  		Required if JETTY_BASE not in the environment
	  		
	  -h  This help message
	  -i  Reset the application. Pull source from master, re-seed db
	  -j  If set, will deploy to inplace manven-jetty. Use -p to set different port
	  -l  Load sample data
	  -p portnum

ENDOFMESSAGE

}

# [-b [-d app_dir]] [-il] [-j [-p port_num]] [-h]

JETTY_APPS=
PORT=8080

PACKAGE_WAR=
INIT_APP=
EMBED_JETTY=
DBLOAD=

while getopts bd:ijlp:h opt; do
  case $opt in
  b)
	PACKAGE_WAR=true
	;;
  d)
	JETTY_APPS=$OPTARG
	echo "WebApps Dir: ${JETTY_APPS}"
	;;
  h)
	usage
	exit
	;;
  i)
	INIT_APP=true
	;;
  j)
	EMBED_JETTY=true
	
	;;
  l)
	DBLOAD=true
	;;
  p)
	PORT=$OPTARG
	echo "PORT: ${PORT}"
	;;
  \?)
    echo "Invalid option: -$OPTARG" >&2
    usage
    exit 
    ;;
  :)
    echo "Option -$OPTARG requires an argument." >&2
    usage
    exit
    ;;
  esac
done

JETTY_APPS=${JETTY_APPS:-"${JETTY_BASE}"}
#JETTY_APPS=${JETTY_APPS:-"`dirname ../target`"}

if [ -z "$EMBED_JETTY" ] && [ ! -d "$JETTY_APPS" ]; then
	echo "directory '${JETTY_APPS}' does not exist!"
	exit 1
fi

if [ ! -z $EMBED_JETTY ]; then
	res=$(nc -z 127.0.0.1 "${PORT}")
	if [[ ! -z $res ]]; then
		echo "Port ${PORT} in use!!"
		exit 1
	fi
fi

SECS=5

package_war()
{
	echo "Create WAR file"
	sudo rm target/patoo-0.1.war
	mvn clean package -DskipTests=true
	if [ -d "${JETTY_APPS}/webapps" ]; then
		JETTY_APPS="${JETTY_APPS}/webapps"
	fi
	echo "Deploying to: ${JETTY_APPS}/ "
	sudo chown apache:apache target/patoo-0.1.war
	sudo mv target/patoo-0.1.war ${JETTY_APPS}/
}

run_jetty()
{
	echo "Use embedded Jetty"
	mvn clean compile jetty:run
}


init_app()
{
	echo "Pull latest source code"
	#git checkout master
	git reset --hard HEAD ; git clean -fd
	roo script --file resources/patoo.roo
}

if [[ ! -z $INIT_APP ]]; then
	init_app
fi

if [[ ! -z $PACKAGE_WAR ]]; then
	if [[ ! -z $DBLOAD ]]; then
		echo 'drop database if exists patoo; create database patoo;' | mysql -uroot
	fi
	package_war
fi

if [[ ! -z $EMBED_JETTY ]]; then
	if [[ ! -z $DBLOAD ]]; then
		echo 'drop database if exists patoo; create database patoo;' | mysql -uroot
	fi
	run_jetty &
fi

if [[ ! -z $DBLOAD ]]; then
	until res=$(nc -z 127.0.0.1 ${PORT})
	do
		sleep ${SECS}
	done

	sleep ${SECS}
	echo "${res}"
	echo "Load DB demo data"
	resources/load_db_base.sh
fi

