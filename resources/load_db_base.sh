#!/usr/bin/env bash
####################################
# Load basic data for Patoo database
####################################

# Database Name
databaseName='patoo'
####################################

# Directory of CSV files
rsrc_dir='/tmp/'
ext='txt'
####################################

# tables='role country parish neighborhood shop_type department shop app_user app_user_roles account abstract_address product shop_item'

# Tables to load. Order is *important*.
tables='rate role country parish neighborhood shop_type department shop app_user app_user_roles account abstract_address product shop_item'
####################################

# absolutePath(dirname $0)
resource_dir=$(cd -P -- "$(dirname -- "$0")" && printf '%s\n' "$(pwd -P)")
####################################

## Copy given resource files to directory readable by
## user: _mysql
for line in `ls ${resource_dir}/*.csv`; do
	fn=`basename ${line}`
	dest=${rsrc_dir}${fn/csv/txt}; 
	
#	if [ ! -f $dest ]; then
		cp $line $dest
		echo "copied '${line}' to '${dest}'"
#	fi
done
####################################
# Below are the column mappings of the given CSV files.

# Rates.
rate_cols='(created,exchange_rate, per_km, sales_tax, service_fee, version)'
####################################

# Roles. The file may be pared down as desired.
role_cols='(name, version)'
####################################

# Countries. The file may be pared down as desired.
country_cols='(country_code, name, version)'
####################################

# Parishes/States. Currently, values for Jamaica and U.S..
parish_cols='(abbrev, area_km_sq, capital, name, population, country, version)'
####################################

# *Deprecated*
us_states_cols='(name, country, abbrev, version)'
####################################

# Neighborhoods. Only Kingston districts thus
# far. Not required for billing.
# Treated here, as only coloquial. 
neighborhood_cols='(name, parish, version)'
####################################

# Sample Shop data
shop_cols='(id, name, phone, postal_code, street, suite, town, type, version, country, neighborhood, parish)'
####################################

# Sample simple user
app_user_cols='(created, login, password, timezone, timezone_offset,version,@enabled) set enabled=cast(@enabled as signed); show warnings;'
####################################

# Sample simple user
app_user_roles_cols='(users,roles)'
####################################

# Account sample data
# 1	2015-07-01 10:11:14	ronniedz@gmail.com	Ronald	Dennison	1	1	\N	1																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																							
account_cols='(id, created, email, firstname, lastname, salutation, version, billing_address, owner)'
####################################

# BillingAddress sample data		
# BillingAddress	1	5	4152300521	94111	450 Sansome Street, 12th Floor	San Francisco	0	US	19	1	11
abstract_address_cols='(dtype, id, apt, phone, postal_code, street, town, version, country, parish, account, neighborhood)'
####################################

# Sample Product
product_cols='(id, hash_name_brand_units, brand, description, image, name, num_units, sku, version, department, @taxable) set taxable=cast(@taxable as signed); show warnings;'
####################################
 
# Sample ShopItem
shop_item_cols='(id, price, priced, version, product, shop, @special) set special=cast(@special as signed); show warnings;'

####################################
 
# Sample ShopType
shop_type_cols='(id, name, version)'
####################################
 
# Sample Department
department_cols='(name, description, version)'
####################################

# Populate Database.
for table in $tables; do

	echo "Table: ${table}"

	# Use table name to compose column variable.
	cols=${table}_cols
	# Get value of created variable
	cols=${!cols}
	
	echo "LOAD DATA INFILE '${rsrc_dir}${table}.${ext}' INTO TABLE ${table} \
fields terminated by '\\t' enclosed by '|' ${cols} "
    
    
	echo "LOAD DATA INFILE '${rsrc_dir}${table}.${ext}' INTO TABLE ${table} \
fields terminated by '\\t' enclosed by '|' ${cols} " | mysql -uroot ${databaseName};

case "$table" in
	account)
		;;
	abstract_address)
		echo "update account set billing_address=1 where id=2"
		echo "update account set billing_address=1 where id=2" | mysql -uroot ${databaseName};
		echo "update account set billing_address=3 where id=3"
		echo "update account set billing_address=3 where id=3" | mysql -uroot ${databaseName};
		;;
esac	
done
